package uj.java.pwj2019.delegations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class Calc {

    BigDecimal calculate(String start, String end, BigDecimal dailyRate) throws IllegalArgumentException {
        var dateStart = LocalDate.parse(start.split(" ")[0]);
        var dateEnd = LocalDate.parse(end.split(" ")[0]);
        var timeStart = LocalTime.parse(start.split(" ")[1]);
        var timeEnd = LocalTime.parse(end.split(" ")[1]);
        var zoneStart = ZoneId.of(start.split(" ")[2]);
        var zoneEnd = ZoneId.of(end.split(" ")[2]);

        var firstDate = ZonedDateTime.of(dateStart, timeStart, zoneStart);
        var secondDate = ZonedDateTime.of(dateEnd, timeEnd, zoneEnd);

        var minDiff = ChronoUnit.MINUTES.between(firstDate, secondDate);
        int minutesInDay = 1440;
        int minutesInHour = 60;
        var dayDiffPay = minDiff / minutesInDay;
        var hourDiffPay = (minDiff % minutesInDay) / minutesInHour;
        var minDiffPay = (minDiff % minutesInDay) % minutesInHour;

        var toPay = dailyRate.multiply(new BigDecimal(dayDiffPay));

        int lowerLimit = 8;
        int upperLimit = 12;
        if(minDiff <= 0) {
            return new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
        }
        if(hourDiffPay > 0) {
            if (hourDiffPay <= lowerLimit) {
                var tmp = dailyRate.divide(new BigDecimal(3), 2, RoundingMode.HALF_UP);
                toPay = toPay.add(tmp);
            } else if (hourDiffPay <= upperLimit) {
                var tmp = dailyRate.divide(new BigDecimal(2), 2, RoundingMode.HALF_UP);
                toPay = toPay.add(tmp);
            } else {
                toPay = toPay.add(dailyRate);
            }
        }else if(minDiffPay > 0) {
            var tmp = dailyRate.divide(new BigDecimal(3), 2, RoundingMode.HALF_UP);
            toPay = toPay.add(tmp);
        }
        return toPay.setScale(2, RoundingMode.HALF_UP);
    }
}
